# 정답

## 1

## 2

### 1

```sql
INSERT INTO `employee` (`name`, `salary`, `role`)
VALUES ('Alice', 5367, 'Developer');
```

### 2

#### 1

```sql
SELECT *
FROM `employee`;
```

#### 2

```sql
SELECT *
FROM `employee`
WHERE `name` = 'Bob';
```

#### 3

```sql
SELECT `salary`
FROM `employee`
WHERE `name` = 'Carol';
```

#### 4

```sql
SELECT *
FROM `employee`
WHERE `salary` >= 5000;
```

#### 5

```sql
SELECT `name`, `role`
FROM `employee`
WHERE `salary` >= 7000;
```

### 3

#### 1

```sql
UPDATE `employee`
SET `role` = 'Manager'
WHERE `name` = 'Dave';
```

#### 2

```sql
UPDATE `employee`
SET `role` = 'Security Engineer'
WHERE `role` = 'Hacker';
```

### 4

#### 1

```sql
SELECT *
FROM `employee`
ORDER BY `salary` DESC;
```

#### 2

```sql
SELECT *
FROM `employee`
WHERE `role` = 'Developer'
ORDER BY `salary` ASC;
```

### 5

#### 1

```sql
SELECT *
FROM `employee`
ORDER BY `salary` DESC
LIMIT 3;
```

#### 2

```sql
SELECT *
FROM `employee`
WHERE `role` = 'Developer'
ORDER BY `salary` ASC
LIMIT 3;
```

### 6

#### 1

```sql
SELECT *
FROM `employee`
ORDER BY `salary` DESC
LIMIT 3
OFFSET 3;
```

#### 2

```sql
SELECT *
FROM `employee`
WHERE `role` = 'Security Engineer'
ORDER BY `salary` ASC
LIMIT 3
OFFSET 3;
```

### 7

#### 1

```sql
SELECT DISTINCT `role`
FROM `employee`;
```

### 8

#### 1

```sql
SELECT `role`, COUNT(*)
FROM `employee`
GROUP BY `role`;
```

#### 2

```sql
SELECT `role`, AVG(`salary`)
FROM `employee`
GROUP BY `role`;
```

### 9

#### 1

```sql
DELETE
FROM `employee`
WHERE `name` = 'Eve';
```

#### 2

```sql
DELETE
FROM `employee`
WHERE `salary` <= 3000;
```

## 3

### 4

#### 1

```sql
SELECT *
FROM `employee`;
```

#### 2

```sql
SELECT *
FROM `team`;
```

#### 3

```sql
SELECT e.name, t.name
FROM employee e
  INNER JOIN team t
  ON e.team = t.id;
```

## 4

### 2

#### 1

```sql
SELECT c.name
FROM student s
  INNER JOIN student_club sc
  ON s.id = sc.student
    INNER JOIN club c
    ON sc.club = c.id
WHERE s.name = 'Alice';
```

#### 2

```sql
SELECT s.name
FROM student s
  INNER JOIN student_club sc
  ON s.id = sc.student
    INNER JOIN club c
    ON sc.club = c.id
WHERE c.name = 'Hacking';
```
